<?php
include("../database/database.php");
$res = null;

if (isset($_REQUEST['id'])) {
    $resul = mysqli_query($conn, "SELECT * FROM `usuaris` WHERE id = '$_REQUEST[id]'");
    $res = mysqli_fetch_array($resul);
}
if ($res) {
    ?>
    <form action="modificarUsuari.proc.php" method="GET">
        <table>
            <input type="hidden" name="id" value="<?php echo $res['id']; ?>">
            <tr>
                <td>Nom:</td>
                <td><input name="usu_nom" size="10" value="<?php echo $res['usu_nom']; ?>"></td>
            </tr>
            <tr>
                <td>Contrasenya:</td>
                <td><input type="password" name="usu_password" size="100" value="<?php echo $res['usu_password']; ?>"></td>
            </tr>
            <tr>
                <td>Nivell:</td>
                <td><select name="usu_nivell">
                        <option value="admin">admin</option>
                        <option value="user">user</option>
                    </select></td>
            </tr>
            <tr>
                <td colspan="2"><input type="submit" value="Enviar"></td>
            </tr>
        </table>
    </form>
    <?php
} else {
    echo "<h1>ERROR!</h1>";
}
?>
<style>
    form {
        font-family: sans-serif;
        margin: 20px;
        padding: 20px;
        border: 1px solid #ccc;
        border-radius: 5px;
        background-color: #f5f5f5;
    }

    table {
        border-collapse: collapse;
        width: 100%;
    }

    td,
    th {
        padding: 8px;
        text-align: left;
        border-bottom: 1px solid #ddd;
    }

    tr:hover {
        background-color: #f5f5f5;
    }

    input[type=text],
    input[type=password] {
        width: 100%;
        padding: 12px 20px;
        margin: 8px 0;
        box-sizing: border-box;
        border: 2px solid #ccc;
        border-radius: 4px;
        background-color: #f8f8f8;
    }

    input[type=submit] {
        background-color: #4CAF50;
        color: white;
        padding: 12px 20px;
        border: none;
        border-radius: 4px;
        cursor: pointer;
        float: right;
    }

    input[type=submit]:hover {
        background-color: #45a049;
    }
</style>