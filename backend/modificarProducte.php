<?php
    include("../database/database.php");

    $resul = mysqli_query($conn, "SELECT * FROM productes WHERE prod_id='$_REQUEST[prod_id]'");
    if(mysqli_num_rows($resul)==1){
        $res=mysqli_fetch_array($resul);
?>
    <form action="modificarProducte.proc.php" method="POST"  enctype="multipart/form-data">
            <table>
                <input type="hidden" name="prod_id" value="<?php echo $res['prod_id'];?>">
                <tr>
                    <td>Nom producte:</td>
                    <td><input name="prod_nom" size="20" value="<?php echo $res['prod_nom']; ?>" required></td>
                </tr>
                <tr>
                    <td>Descripció: </td>
                    <td><input type="textarea" name="prod_desc" value="<?php echo $res['prod_desc']; ?>" required></td>
                </tr>
                <tr>
                    <td>Preu: </td>
                    <td><input name="prod_preu" type="number" min=1 max=99999 value="<?php echo $res['prod_preu']; ?>" step=".01" required></td>
                </tr>
                <tr>
                    <td>Imatge: </td>
                    <td><input type="file" name="prod_img" required></td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" value="Modificar"></td>
                </tr>
            </table>
        </form>
        <?php        
    } else {
        header("localhost: ./error.php?producteInexistent");
    }
?>
<?php
    
?>
<style>
    h1{
        text-transform: uppercase;
        padding: 5%;
    }
    body {
        font-family: sans-serif;
        margin: 0;
        padding: 0;
    }

    h1 {
        text-align: center;
    }

    table {
        width: 100%;
        border-collapse: collapse;
        margin: 20px 0;
    }

    th,
    td {
        padding: 10px;
        border: 1px solid #ccc;
    }

    th {
        background-color: #f2f2f2;
        font-weight: bold;
    }

    td {
        text-align: center;
    }

    tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    a {
        text-decoration: none;
        color: #000;
        padding: 5px 10px;
        background-color: #ccc;
        border-radius: 3px;
    }

    a:hover {
        background-color: #4CAF50;
        color: #fff;
    }

    .insertar {
        text-align: right;
    }

    .insertar a {
        background-color: #4CAF50;
        color: #fff;
    }
</style>