
<?php
    session_start();
    if(!isset($_SESSION['usu_nom'])){
        session_destroy();
            header('location: ./login.php?error');
    } else if (isset($_SESSION['usu_nivell'])) {
            header('location: ./error.php?permission');
    } else {
        echo $_SESSION['usu_nom'];
        include("../database/database.php");
?>
    <table>
        <th colspan=2>Gestió</th>
        <tr>
            <td><a href="gestioUsuaris.php">Gestionar usuaris</a></td>
            <td><a href="./gestioProductes.php">Gestionar productes</a></td>
        </tr>
    </table>
<?php
        }
?>
<style>
    h1{
        text-transform: uppercase;
        padding: 5%;
    }
    body {
        font-family: sans-serif;
        margin: 0;
        padding: 0;
    }

    h1 {
        text-align: center;
    }

    table {
        width: 100%;
        border-collapse: collapse;
        margin: 20px 0;
    }

    th,
    td {
        padding: 10px;
        border: 1px solid #ccc;
    }

    th {
        background-color: #f2f2f2;
        font-weight: bold;
    }

    td {
        text-align: center;
    }

    tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    a {
        text-decoration: none;
        color: #000;
        padding: 5px 10px;
        background-color: #ccc;
        border-radius: 3px;
    }

    a:hover {
        background-color: #4CAF50;
        color: #fff;
    }

    .insertar {
        text-align: right;
    }

    .insertar a {
        background-color: #4CAF50;
        color: #fff;
    }
</style>