<?php
session_start();

if (!isset($_SESSION['usu_nom'])) {
    session_destroy();
    header('Location: gestioUsuaris.php?error=Has de logarte per entrar a l\'espai personal!');
    
} 

include("../database/database.php");

?>
<h1>Gestió d'usuaris</h1>
<table>
    <tr>
        <th>NOM</th>
        <th>NIVELL</th>
        <th colspan="2">OPERACIONS</th>
    </tr>

    <?php

    $sql = "SELECT usu_nom, usu_nivell FROM usuari";
    $resul = mysqli_query($conn, $sql);

    while ($res = mysqli_fetch_array($resul)) {
        echo "<tr>
            <td>$res[usu_nom]</td>
            <td>$res[usu_nivell]</td>
            <td><a href='modificarUsuari.php?usu_nom=$res[usu_nom]'>Modificar</a></td>
            <td><a href='eliminarUsuari.proc.php?usu_nom=$res[usu_nom]'>Eliminar</a></td>
            </tr>";
    }
    echo "<tr><td colspan='2'><a href='crearUsuari.php'>Crear</a></td> <td colspan='2'><a href='logout.proc.php'>Logout</a></td></tr>";
    mysqli_close($conn);

    ?>
</table>
<style>
    h1{
        text-transform: uppercase;
        padding: 5%;
    }
    body {
        font-family: sans-serif;
        margin: 0;
        padding: 0;
    }

    h1 {
        text-align: center;
    }

    table {
        width: 100%;
        border-collapse: collapse;
        margin: 20px 0;
    }

    th,
    td {
        padding: 10px;
        border: 1px solid #ccc;
    }

    th {
        background-color: #f2f2f2;
        font-weight: bold;
    }

    td {
        text-align: center;
    }

    tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    a {
        text-decoration: none;
        color: #000;
        padding: 5px 10px;
        background-color: #ccc;
        border-radius: 3px;
    }

    a:hover {
        background-color: #4CAF50;
        color: #fff;
    }

    .insertar {
        text-align: right;
    }

    .insertar a {
        background-color: #4CAF50;
        color: #fff;
    }
</style>