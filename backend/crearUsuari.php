<form action="crearUsuari.proc.php" method="GET">
    <table>
        <tr>
            <td>Usuari:</td>
            <td><input name="usu_nom" size="100"></td>
        </tr>
        <tr>
            <td>Contrasenya:</td>
            <td><input type="password" name="usu_password" size="100"></td>
        </tr>
        <tr>
            <td>Nivell:</td>
            <td><select name="usu_nivell" >
                    <option value="admin">admin</option>
                    <option value="user">user</option>
                </select></td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" value="Enviar"></td>
        </tr>
    </table>
</form>
<style>
    body {
        font-family: sans-serif;
        margin: 0;
        padding: 0;
    }

    h1 {
        text-align: center;
    }

    table {
        width: 100%;
        border-collapse: collapse;
        margin: 20px 0;
    }

    th,
    td {
        padding: 10px;
        border: 1px solid #ccc;
    }

    th {
        background-color: #f2f2f2;
        font-weight: bold;
    }

    td {
        text-align: center;
    }

    tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    a {
        text-decoration: none;
        color: #000;
        padding: 5px 10px;
        background-color: #ccc;
        border-radius: 3px;
    }

    a:hover {
        background-color: #4CAF50;
        color: #fff;
    }

    .insertar {
        text-align: right;
    }

    .insertar a {
        background-color: #4CAF50;
        color: #fff;
    }
</style>