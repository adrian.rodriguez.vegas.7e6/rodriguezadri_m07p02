<?php


    include("../database/database.php");
    session_start();
    if (!isset($_SESSION['usu_nom'])){
    
        echo "<a href='login.php'>Login</a>";
        
    } else {
       
        echo $_SESSION['usu_nom'];
        
    }
?>

    <table>
    <tr>
        <th>ID</th>
        <th>NOM</th>
        <th>DESCRIPCIÓ</th>
        <th>PREU</th>
        <th>IMATGE</th>
    </tr>

    <?php

        $result = mysqli_query($conn, "SELECT * FROM productes ORDER BY prod_id");
        
        while($res = mysqli_fetch_array($result)){
            $query = "SELECT prod_id FROM productes WHERE prod_id = $res[prod_id]";
            $result2 = mysqli_query($conn, $query);
            $dades = mysqli_fetch_array($result2);
            if ($dades['prod_id'] == true){
                echo "<tr>
                    <td>$res[prod_id]</td>
                    <td>$res[prod_nom]</td>
                    <td>$res[prod_des]</td>
                    <td>$res[prod_preu]€</td>
                    <td><img src='../src/$res[prod_img]' height='80px' width='80px'></td>
                </tr>";
            }
        }
        echo "</table>";
        mysqli_close($conn);
?>
<style>
    h1{
        text-transform: uppercase;
        padding: 5%;
    }
    body {
        font-family: sans-serif;
        margin: 0;
        padding: 0;
    }

    h1 {
        text-align: center;
    }

    table {
        width: 100%;
        border-collapse: collapse;
        margin: 20px 0;
    }

    th,
    td {
        padding: 10px;
        border: 1px solid #ccc;
    }

    th {
        background-color: #f2f2f2;
        font-weight: bold;
    }

    td {
        text-align: center;
    }

    tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    a {
        text-decoration: none;
        color: #000;
        padding: 5px 10px;
        background-color: #ccc;
        border-radius: 3px;
    }

    a:hover {
        background-color: #4CAF50;
        color: #fff;
    }

    .insertar {
        text-align: right;
    }

    .insertar a {
        background-color: #4CAF50;
        color: #fff;
    }
</style>