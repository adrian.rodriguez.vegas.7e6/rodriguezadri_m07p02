<?php
    include("../includes/head.html");
    session_start();
    if(!isset($_SESSION['usu_nom'])){
        session_destroy();
        header('location: ./login.php?error');
    } else {
        echo $_SESSION['usu_nom'];
        include("../database/database.php");
    ?>
<form action="./crearProducte.proc.php" method="POST" enctype="multipart/form-data">
    <table>
        <tr>
            <td>Nom del producte: </td>
            <td><input name="prod_nom" size="20" required></td>
        </tr>
        <tr>
            <td>Descripció del producte: </td>
            <td><textarea name="prod_desc" rows="4" cols="50" required></textarea></td>
        </tr>
        <tr>
            <td>Preu: </td>
            <td><input name="prod_preu" type="number" min=1 max=99999 step=".01" required></td>
        </tr>
        <tr>
            <td>Imatge: </td>
            <td><input type="file" name="prod_img" required></td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" value="Crear"></td>
        </tr>
    </table>
</form>
<?php
    }
    include("../includes/foot.html");
?>